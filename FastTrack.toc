## Interface: 50001

## Title: FastTrack
## Notes: Do your mundane things fast, like auto sell poor items, auto accept binding loot, and quick display of session profit/loss.
## Version: 1.0
## Author: WapakFrenzy (Frazoid@Bronzebeard || Naughty & Nice)
## SavedVariablesPerCharacter: FastTrack_History
## Dependencies:
main.lua
FastTrack.xml