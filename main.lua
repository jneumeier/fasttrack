FastTrack_LastMoney = 0
FastTrack_MadeProfit = false
FastTrack_History = {}

function FastTrack_OnMouseDown()
	FastTrack_MainFrame:StartMoving()
end

function FastTrack_OnMouseUp()
	FastTrack_MainFrame:StopMovingOrSizing()
end

function FastTrack_OnLoad()
	FastTrack_ChangeSize()
	FastTrack_MainFrame:RegisterEvent("PLAYER_MONEY")
	FastTrack_MainFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
	FastTrack_MainFrame:RegisterEvent("MERCHANT_SHOW")
	FastTrack_MainFrame:RegisterEvent("CONFIRM_LOOT_ROLL")
	FastTrack_MainFrame:RegisterEvent("LOOT_BIND_CONFIRM")
end

function FastTrack_ChangeSize()
	local width = 65
	
	width = width + FastTrack_MainFrame_GoldString:GetStringWidth()
	width = width + FastTrack_MainFrame_SilverString:GetStringWidth()
	width = width + FastTrack_MainFrame_CopperString:GetStringWidth()

	FastTrack_MainFrame:SetWidth(width)	
end

function FastTrack_FastPartyLoot(rollID, roll)
	DEFAULT_CHAT_FRAME:AddMessage("FastTrack has not yet implemented FastPartyLoot (yet) ["..rollID..", "..roll.."].");
end

function FastTrack_FastSoloLoot(slotID)
	for i = 1, STATICPOPUP_NUMDIALOGS do
		local frame = _G["StaticPopup"..i];
		if frame.which == "LOOT_BIND" and frame:IsVisible() then
			StaticPopup_OnClick(frame, 1);
		end
	end
	FastTrack_MainFrame:UnregisterEvent("LOOT_BIND_CONFIRM");
	LootSlot(slotID);
	for i = 1, STATICPOPUP_NUMDIALOGS do
	    local frame = _G["StaticPopup"..i]
	    if frame.which == "LOOT_BIND" and frame:IsVisible() then
	    	StaticPopup_OnClick(frame, 1)
	   	end
	end
	ConfirmLootSlot(slotID)
	FastTrack_MainFrame:RegisterEvent("LOOT_BIND_CONFIRM")
	
	DEFAULT_CHAT_FRAME:AddMessage("FastTrack automatically confirmed the looting of a bindable item ["..slotID.."].");
end

function FastTrack_FastSell(qual)

	-- We want to be able to send in the max quality we want to be able to sell fast.
	-- FastSell(0) will only sell items of poor quality or less.
	-- Quality is 0 - 5 (poor to legendary)
	
	local itemLink, myName, myQual, myLevel, myType, mySellPrice, myStack, numSlots;
	local gold, silver, copper;
	local totalSell = 0;
	local totalCount = 0;
	
	-- DEFAULT_CHAT_FRAME:AddMessage("Attempting to FastSell your poor items...");
	
	for bagid = 0, 4 do
		numSlots = GetContainerNumSlots(bagid);
		for slotid = 1, numSlots do
			-- Loops through each slot in each bag
			_,myStack,_,_,_,_,itemLink = GetContainerItemInfo(bagid,slotid);
			if ( itemLink ~= nil) then
				myName,_,myQual,myLevel,_,myType,_,_,_,_,mySellPrice = GetItemInfo(itemLink);
				if (myQual <= qual) then
					UseContainerItem(bagid,slotid);
					totalSell = totalSell + (mySellPrice * myStack);
					totalCount = totalCount + myStack;
					gold, silver, copper = FastTrack_ReturnMoney(mySellPrice * myStack);
					DEFAULT_CHAT_FRAME:AddMessage(  "FastSell vendored " ..myStack.." |cFF00FF00" .. myName ..
													"|r: for |cFFFFFF00" .. gold ..
													" |cFFAAAAAA" .. silver ..
													" |cFF993300" .. copper .."|r!");
				end
			end
		end			
	end

	gold, silver, copper = FastTrack_ReturnMoney(totalSell);
	if totalSell ~= 0 then
		DEFAULT_CHAT_FRAME:AddMessage(	"Your total FastSell of " .. totalCount .. 
										" items was a profit of: |cFFFFFF00" .. gold ..
										" |cFFAAAAAA" .. silver ..
										" |cFF993300" .. copper .."|r!");
	else
		-- Nothing sold this vendor trip
		DEFAULT_CHAT_FRAME:AddMessage(  "You had no poor items to sell to this vendor.")
	end
end

function FastTrack_OnEvent(self, event, ...)
	if event == "PLAYER_ENTERING_WORLD" then
		FastTrack_LastMoney = GetMoney()
	end
	
	if event == "MERCHANT_SHOW" then
		FastTrack_FastSell(0)
	end

	if event == "CONFIRM_LOOT_ROLL" then
		local rollID = select(1, ...);
		local roll = select(2, ...);
		FastTrack_FastPartyLoot(rollID, roll);
	end
		
	if event == "LOOT_BIND_CONFIRM" then
		local slotID = select(1, ...);
		FastTrack_FastSoloLoot(slotID);
	end

	if event == "PLAYER_MONEY" then
		if not FastTrack_MadeProfit then
			table.insert(FastTrack_History, 1, 0)
			FastTrack_MadeProfit = true
		end

		local difference = GetMoney() - FastTrack_LastMoney
		FastTrack_History[1] = FastTrack_History[1] + difference
		FastTrack_LastMoney = GetMoney();

		local gold, silver, copper = FastTrack_ReturnMoney(FastTrack_History[1])
		
		FastTrack_MainFrame_GoldString:SetText(gold)
		FastTrack_MainFrame_SilverString:SetText(silver)
		FastTrack_MainFrame_CopperString:SetText(copper)

		FastTrack_ChangeSize()
		
		if FastTrack_History[1] < 0 then
			FastTrack_MainFrame_GoldString:SetTextColor(1,0,0)
			FastTrack_MainFrame_SilverString:SetTextColor(1,0,0)
			FastTrack_MainFrame_CopperString:SetTextColor(1,0,0)
		elseif FastTrack_History[1] > 0 then
			FastTrack_MainFrame_GoldString:SetTextColor(0,1,0)
			FastTrack_MainFrame_SilverString:SetTextColor(0,1,0)
			FastTrack_MainFrame_CopperString:SetTextColor(0,1,0)
		else
			FastTrack_MainFrame_GoldString:SetTextColor(1,1,0)
			FastTrack_MainFrame_SilverString:SetTextColor(1,1,0)
			FastTrack_MainFrame_CopperString:SetTextColor(1,1,0)
		end
	end
end

function FastTrack_ReturnMoney(money)
	local absMoney = abs(money)
	local gold = floor(absMoney / 10000)
	local silver = floor((absMoney - gold*10000) / 100)
	local copper = absMoney - gold*10000 - silver*100
	return gold, silver, copper
end

function FastTrack_Button_OnClick()
	local alltime = 0;
	
	for k,v in pairs(FastTrack_History) do
		alltime = alltime + v
	end

	local gold, silver, copper = FastTrack_ReturnMoney(alltime)
	
	if alltime < 0  then
		DEFAULT_CHAT_FRAME:AddMessage("FastTrack - All Time |cFFFF0000deficit|r: ")
	else	
		DEFAULT_CHAT_FRAME:AddMessage("FastTrack - All Time |cFF00FF00profit|r: ")
	end
	
	DEFAULT_CHAT_FRAME:AddMessage(	"|cFFFFFF00" .. gold .. 
									" |cFFAAAAAA" .. silver ..
									" |cFF993300" .. copper)
end

